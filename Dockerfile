FROM openjdk
ADD target/netflix-zuul-api-gateway-server-0.0.1-SNAPSHOT.jar.original conntainerized-gateway.jar
ENTRYPOINT ["java", "-jar", "/containerized-gateway.jar"]
EXPOSE 8766
